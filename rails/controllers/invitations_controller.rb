class InvitationsController < ApplicationController
  before_action :set_invitation, only: [:resend, :cancel, :destroy]
  skip_before_action :authenticate_user!, only: [:accept]

  # GET /invitations
  def index
    @invitations = current_user.invitations
  end

  # POST /invitations
  def create
    @invitation = current_user.invitations.new(invitation_params)
    respond_to do |format|
      if @invitation.save
        format.html { redirect_to invitations_path, notice: 'Invitación creada' }
        format.json { render :show, status: :ok }
      else
        format.html { redirect_to invitations_path, alert: @invitation.errors.full_messages.join(", ") }
        format.json { render json: @invitation.errors, status: :unprocessable_entity }
      end
    end
  end

  def resend
    if @invitation.recent?
      flash[:danger] = "La invitacion fue enviada hace poco, espera que la respondan"
    else
      @invitation.resend!
      flash[:notice] = "Invitacion reenviada"
    end
    redirect_to invitations_path
  end

  def cancel
    @invitation.cancel!
    redirect_to invitations_path, notice: "Invitacion cancelada"
  end

  def destroy
    @invitation.destroy
    redirect_to invitations_path, notice: "Invitacion eliminada"
  end

  def accept
    promo_code = params[:promocode]
    pc = PromoCode.active.find_by(code: promo_code)
    if pc
      flash[:notice] = "Aceptaras la invitacion de #{pc.user.profile.full_name}"
    elsif promo_code.present?
      flash[:danger] = "El codigo ingresado no es valido"
    end
    redirect_to new_user_registration_url(promocode: pc.try(:code))
  end

  private

  def set_invitation
    @invitation = current_user.invitations.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def invitation_params
    params.require(:invitation).permit(:email)
  end
end
