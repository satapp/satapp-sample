class AssignmentControlJob < ApplicationJob
  queue_as :default

  def perform(trip_id:)
    Trips::TripService.control_assignment(trip_id: trip_id)
  rescue => e
    assignment_control_logger.fatal "Error en assignment_control del trip #{trip_id}: #{e.message}"
    assignment_control_logger.debug e.backtrace
  end

  def assignment_control_logger
    @assignment_control_logger ||= Logger.new("#{Rails.root}/log/assignment_control.log")
  end
end
