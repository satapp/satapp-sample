# == Schema Information
#
# Table name: zones
#
#  id                            :bigint(8)        not null, primary key
#  name                          :string
#  geometry                      :geography({:srid geometry, 4326
#  time_per_km                   :decimal(, )
#  route_km_per_linear_km        :decimal(, )
#  kms_to_base_ratio             :decimal(, )
#  waits_to_base_ratio           :decimal(, )
#  buffer_time                   :decimal(, )
#  variation_periods             :decimal(, )
#  increment_pct_per_period      :decimal(, )
#  min_radius                    :decimal(, )
#  max_distance_to_meeting_point :decimal(, )
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  min_price                     :decimal(, )
#  minimal_wait                  :integer
#  min_salary                    :decimal(, )
#  min_salary_long_trip          :decimal(, )
#  max_negative_price            :decimal(, )
#

class Zone < ApplicationRecord

  def point_list
    return nil unless self.geometry.present?
    [self.geometry.coordinates.first.map { |x| {lat: x[1], lng: x[0]} }]
  end

  def polygon_centroid
    return nil unless self.geometry.present?

    query = "SELECT ST_AsText(ST_centroid('#{self.geometry}')) point"
    result = ActiveRecord::Base.connection.exec_query(query)
    #coords = result.first["point"].gsub("POINT(","").gsub(")","").split(" ")
    coords = result.first["point"].scan(/^POINT\((\-?\d+\.\d+?)\s(\-?\d+\.\d+?)\)$/).flatten.map(&:to_f)
    {lat: coords[1], lng: coords[0]}
  end

  def self.find_covering(coordinates)
    Zone.where('ST_Covers(geometry, ?)', coordinates).take
  end

  def pricing_data
    {
      kms_to_base_ratio: self.kms_to_base_ratio,
      waits_to_base_ratio: self.waits_to_base_ratio,
      time_per_km: self.time_per_km,
      route_km_per_linear_km: self.route_km_per_linear_km,
      buffer_time: self.buffer_time,
      min_radius: self.min_radius,
      min_price: self.min_price,
      min_salary: self.min_salary,
      min_salary_long_trip: self.min_salary_long_trip,
      max_negative_price: self.max_negative_price,
      minimal_wait: self.minimal_wait,
      max_distance_to_meeting_point: self.max_distance_to_meeting_point,
      variation_periods: self.variation_periods,
      increment_pct_per_period: self.increment_pct_per_period
    }
  end
end
