# == Schema Information
#
# Table name: legs
#
#  id                    :bigint(8)        not null, primary key
#  trip_id               :bigint(8)
#  user_id               :bigint(8)
#  return_way            :boolean
#  signature             :string
#  barcode_data          :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  with_out_dni          :boolean
#  state                 :string           default("pending")
#  started_at            :datetime
#  canceled_at           :datetime
#  finished_at           :datetime
#  identified_at         :datetime
#  signed_at             :datetime
#  driver_id             :bigint(8)
#  informed_time_start   :datetime
#  informed_time_end     :datetime
#  informed_time_elapsed :integer
#  informed_prev_state   :string
#  interruption_address  :string
#  interruption_coords   :geography({:srid point, 4326
#  original_leg_id       :bigint(8)
#  gmaps_info            :jsonb
#  price_info            :jsonb
#

class Leg < ApplicationRecord
  has_one_attached :signature_image
  STATES = {
    pending:     "Pendiente",
    assigned:    "Asignado",
    unassigned:  "Desasignado",
    returned:    "Devuelto",
    initiated:   "Iniciado",
    at_origin:   "En Origen",
    started:     "En viaje",
    finished:    "Finalizado",
    waiting:     "En Espera",
    informed:    "Informado",
    canceled:    "Negativo",
    interrupted: "Interrumpido",
    set_dni:     "Pasajero Identificado",
    signed:      "Traslado firmado",
    buy_time:    "Extension espera negativo",
    wait:        "Art solicitó esperar",
    sms_sent:    "SMS enviado",
    meeting_point_confirmed: "Punto de encuentro confirmado",
    return_requested: "Retorno solicitado"
  }

  IN_PROGRESS_STATES = [
    :initiated,
    :at_origin,
    :started,
    :informed,
    :interrupted
  ].freeze

  ALLOWED_EVENTS = [
    :init,
    :at_origin,
    :start,
    :inform,
    :finish,
    :interrupt,
    :cancel,
    :wait_return,
    :return,
    :restart
  ].freeze

  belongs_to :trip
  belongs_to :user, optional: true
  belongs_to :driver, optional: true
  belongs_to :interrupted_leg, class_name: 'Leg', foreign_key: 'original_leg_id', optional: true

  has_one :resumption_leg, class_name: 'Leg', foreign_key: 'original_leg_id', dependent: :restrict_with_error
  
  has_many :logs, -> { order('created_at DESC') }, class_name: "TripLog", dependent: :destroy
  has_many :inform_reports, -> { order('created_at DESC') }, dependent: :destroy
  has_many :leg_tolls, -> { order('created_at DESC') }, dependent: :destroy

  scope :pending, -> { where(legs: { state: 'pending' }) }
  scope :started, -> { where.not(legs: { state: 'pending' }) }
  scope :informed, -> { where(legs: { state: 'informed' }) }
  scope :finished, -> { where(legs: { state: ['canceled', 'finished'] }) }
  scope :in_progress, -> { where(legs: { state: IN_PROGRESS_STATES.map(&:to_s) }) }
  scope :ended, -> { where(legs: { state: ['canceled', 'finished', 'interrupted'] }) }
  scope :not_interrupted, -> { where.not(legs: { state: 'interrupted' }) }
  scope :assigned, -> { where.not(user_id: nil) }
  scope :unassigned, -> { where(user_id: nil) }
  scope :publicable, -> {
    where(state: 'pending', user_id: nil)
    .joins(:trip)
    .where("(legs.return_way IS TRUE and trips.return_time != '')
            OR (legs.return_way IS FALSE AND trips.trip_date + (trips.trip_hour||':00')::time > now() - interval '5 hours')") # 2 hs de gracia + 3 por la zona horaria
  }

  scope :immediate, -> {
    joins(:trip)
    .where(legs: { return_way: false, state: 'pending', user_id: nil }, trips: { immediate: true })
    .or(
      joins(:trip)
      .where(user_id: nil)
      .where.not(legs: { return_way: false }, trips: { return_time: '' })
    )
  }

  state_machine :state, initial: :pending do
    before_transition on: :init do |leg, transition|
      leg.flow(transition).before_init
    end

    before_transition on: :finish do |leg, transition|
      leg.flow(transition).before_finish
    end

    after_transition on: :wait_return do |leg, transition|
      leg.flow(transition).after_wait_return
    end

    after_transition do |leg, transition|
      leg.flow(transition).log_transition
    end
  
    after_transition on: :init do |leg, transition|
      leg.flow(transition).after_init
    end

    after_transition on: :at_origin do |leg, transition|
      leg.flow(transition).after_at_origin
    end

    after_transition on: :start do |leg, transition|
      leg.flow(transition).after_start
    end

    after_transition on: :inform do |leg, transition|
      leg.flow(transition).after_inform
    end

    after_transition on: :finish do |leg, transition|
      leg.flow(transition).after_finish
    end

    after_transition on: :interrupt do |leg, transition|
      leg.flow(transition).after_interrupt
    end

    after_transition on: :cancel do |leg, transition|
      leg.flow(transition).after_cancel
    end

    after_transition on: :return do |leg, transition|
      leg.flow(transition).after_return
    end

    event :restart do
      transition any => :pending
    end

    # sale de la base
    event :init do
      transition [:pending, :waiting, :informed] => :initiated
    end

     # en origen
    event :at_origin do
      transition [:initiated, :informed] => :at_origin
    end

    # iniciar viaje con pasajero
    event :start do
      transition [:at_origin, :informed] => :started
    end

    # finaliza en el destino
    event :finish do
      transition [:started] => :finished
    end
    
    event :wait_return do
      transition [:pending, :informed] => :waiting
    end

    # informa un problema
    event :inform do
      transition [:waiting, :initiated, :at_origin, :started] => :informed
    end

    # se confirma negativo
    event :cancel do
      transition [:informed] => :canceled
    end

    # se devuelve a feleval
    event :return do
      transition [:pending, :assigned, :waiting] => :returned
    end

    # se iterrumpe por problemas
    event :interrupt do
      transition any => :interrupted
    end
  end

  def gmaps_info
    ActiveSupport::HashWithIndifferentAccess.new(super)
  end

  def price_info
    ActiveSupport::HashWithIndifferentAccess.new(super)
  end

  def price
    current_price = self.price_info[:current_price]
    current_price.present? ? sprintf('%.2f', current_price) : nil
  end

  def in_progress?
    IN_PROGRESS_STATES.include?(self.state.to_sym)
  end

  def flow(transition)
    self.trip.flow_class.new(leg_id: self.id, transition: transition)
  end

  def is_pending?
    self.state == 'pending' || self.state == 'waiting'
  end

  def is_finished?
    [:finished, :canceled].include?(self.state.to_sym)
  end

  def name
    self.return_way ? "Retorno" : "Ida"
  end

  def address_description(way)
    way = if self.return_way
      way == "to" ? "from" : "to"
    else
      way
    end
    self.trip.description(way)
  end
  
  def state_to_string
    STATES[self.state.to_sym]
  end

  def profile
    self.user.try(:profile)
  end

  def driver_info
    return nil unless self.user_id.present?
    [profile.full_name, profile.phone].join(" ")
  end

  def assigned?
    self.user_id.present?
  end

  def assign!(user:)
    transaction do
      self.update!(user_id: user.id)
      self.logs.create(event: "assigned", trip_id: self.trip_id, user_id: user.id)
      assigned = true
    end
  rescue => e
    self.errors.add(:user, e.message)
    assigned = false
  end

  def unassign!(reason_id:)
    unassigned = false

    transaction do
      self.update(user_id: nil, driver_id: nil, identified_at: nil, with_out_dni: nil, barcode_data: nil)
      self.logs.create(event: "unassigned", trip_id: self.trip_id, user_id: self.user_id, cancel_reason_id: reason_id)
      unassigned = true
    end

    unassigned
  end

  def identified
    self.barcode_data.present? || self.with_out_dni
  end

  def startable
    return false if self.user&.current_trip.present?
    return self.assigned? if self.trip.immediate?
    return self.assigned? && self.trip.return_time.present? if self.return_way

    gap = MainConfig.current.gap || 4
    hour, min = self.trip.trip_hour.split(":").map(&:to_i)
    full_time = self.trip.trip_date.to_datetime.change(hour: hour, min: min, offset: "-0300")
    now       = DateTime.now
    ((full_time - gap.hours) <= now) &&  (now < full_time)
  end

  def set_signature!(user, params)
    transaction do 
      self.update(signature: params[:signature], signed_at: params[:signed_at])
      self.logs.create({
        event: 'signed',
        trip_id: self.trip_id,
        user_id: user&.id,
        point: params[:point],
        position_timestamp: params[:timestamp],
        metadata: params[:metadata],
        context: params[:context]
      })
      # after sign, we finish the trip
      self.finish!(data: { user: user, point: params[:point], position_timestamp: params[:timestamp], context: params[:context], metadata: params[:metadata]})
    end
  end

  def set_dni!(user, params)
    transaction do 
      self.update(with_out_dni: params[:with_out_dni], barcode_data: params[:barcode_data], identified_at: params[:identified_at])
      self.logs.create({
        event: "set_dni",
        trip_id: self.trip_id,
        user_id: user&.id,
        point: params[:point],
        position_timestamp: params[:timestamp],
        metadata: params[:metadata],
        context: params[:context]
      })
      # after set dni, we start the trip
      self.start!(data: { user: user, point: params[:point], position_timestamp: params[:timestamp], context: params[:context], metadata: params[:metadata]})
    end
  end

  def move_to(event, data)
    return false if !ALLOWED_EVENTS.include?(event.to_sym)
    self.send(event, data)
  end

  def register_customer_action(action, user)
    center = ::LegNotificationCenter.new(leg: self, user: user)
    center.send(action)
  end

  def find_informed_log
    find_last_log_by_event("informed")
  end

  def find_initiated_log
    find_last_log_by_event("initiated")
  end

  def find_first_initiated_log
    find_first_log_by_event("initiated")
  end

  def find_interrupted_log
    find_last_log_by_event("interrupted")
  end

  def find_finished_log
    find_last_log_by_event("finished")
  end

  def find_waiting_log
    find_last_log_by_event("waiting")
  end

  def resolve_meeting_point_inform(user:)
    event = self.informed_prev_state
    self.move_to(event, data: { user: user })
  end

  def notify_driver(action)
    return unless self.user.present?

    service = FcmService.new
    @title  = LegNotificationCenter::MESSAGES[action.to_sym][:title]
    @body   = LegNotificationCenter::MESSAGES[action.to_sym][:body]
    @state  = LegNotificationCenter::MESSAGES[action.to_sym][:state]

    service.send_leg_notification(self.user.profile, self, { title: @title, body: sprintf(@body, self.trip.full_identification), type: @state })
    service
  end

  def save_finished_inform_data
    diff_time_in_minutes = 0
    if self.informed_time_start
      diff = Time.now - self.informed_time_start # the result is in seconds.
      diff_time_in_minutes = diff / 60           # convert to minutes
    end

    self.update(informed_time_elapsed: diff_time_in_minutes)
    self.save_new_inform_report
  end

  def initialize_informed_fields!(prev_state:)
    case prev_state
    when 'initiated' 
      @old_state = 'init'
    when 'started'
      @old_state = 'start'
    when 'at_origin'
      @old_state = 'at_origin'
    when 'pending'
      @old_state = 'restart'
    when 'waiting'
      @old_state = 'wait_return'
    else
      @old_state = prev_state
    end

    # start new inform period
    now = Time.zone.now
    self.update({
      informed_time_start:    now,
      informed_time_end:      now + 15.minutes,
      informed_time_elapsed:  0,
      informed_prev_state:    @old_state
    })
  end

  def save_new_inform_report
    self.inform_reports.create({
      user:                   self.user,
      informed_time_start:    self.informed_time_start,
      informed_time_end:      self.informed_time_end,
      informed_time_elapsed:  self.informed_time_elapsed,
      informed_prev_state:    self.informed_prev_state
    })
  end

  def all_logs
    TripLog.where(trip_id: self.trip_id)
      .where("trip_logs.leg_id IS NULL OR trip_logs.leg_id = :leg_id", leg_id: self.id)
      .order(created_at: :desc)
  end

  def tolls_total_amount
    self.leg_tolls.sum(:amount)
  end

  def find_original
    return self if self.original_leg_id.blank?

    interrupted_leg.find_original
  end

  def find_current
    return self if resumption_leg.nil?

    resumption_leg.find_current
  end

  private

  def find_first_log_by_event(event)
    self.logs
      .where(event: event)
      .order(updated_at: :asc)
      .first
  end

  def find_last_log_by_event(event)
    self.logs
      .where(event: event)
      .order(updated_at: :desc)
      .first
  end
end
