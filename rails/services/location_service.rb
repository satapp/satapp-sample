class LocationService
  METRIC = "m"

  def initialize(with_token: false)
    @with_token = with_token
  end

  def search(lat, lng)
    radius = config[:radius]
    result_set = $redis.georadius('drivers', lat, lng, radius, METRIC, 'WITHDIST', 'WITHCOORD')
    find_users(result_set)
  end

  def get(key)
    return $redis.geopos('drivers', key) if exists_in_collection(key)
    $redis.zrem 'drivers', key
    nil
  rescue => e
    puts "LOCATION SERVICE ERROR: #{e.message}"
    nil
  end

  def store(key, data)
    curr_pos = data.delete(:position).to_h
    return add_key(key, curr_pos, data[:timestamp]) if curr_pos.any?
    $redis.zrem 'drivers', key
  end

  def add_key(key, pos, timestamp)
    $redis.geoadd("drivers", pos["latitude"], pos["longitude"], key)
    $redis.setex key, config[:expiration], key # expira en 10 minutos
  rescue => e
    puts "LOCATION SERVICE ERROR: #{e.message}"
    0
  end

  def find_users(users_data)
    users_data.each_with_object([]) do |user_data, result|
      key = user_data[0]
      if exists_in_collection(key)
        user_id = key.split("-").last
        user    = User.find(user_id)
        dist    = sprintf('%.2f km', user_data[1].to_f / 1000.0)
        pos     = user_data[2]
        data = {
          key: key,
          name: "Conductor #{user_id}",
          position: { lat: pos.first.to_f, lng: pos.last.to_f },
          address: "#{user.profile.full_name} :: Se encuentra a #{dist}",
          icon: {
            url: url_helper.image_url('car.png'),
            scaledSize: { width: 32, height: 32 },
            labelOrigin: { x: 16, y: 40 }
          },
          label: dist
        }
        data[:token] = user.profile.device_token if @with_token
        result << data
      else
        $redis.zrem('drivers', key)
      end
    end
  end

  def get_user_last_position(user_id)
    key = "user-#{user_id}"

    return {} unless exists_in_collection(key)

    pos = $redis.geopos('drivers', key)&.first

    return {} unless pos

    { lat: pos.first.to_f, lon: pos.last.to_f }
  end

  def exists_in_collection(key)
    $redis.get key
  end

  def url_helper
    @url_helper ||= ActionController::Base.helpers
  end

  def config
    @config ||= MainConfig.current.location_service_data
  end
end