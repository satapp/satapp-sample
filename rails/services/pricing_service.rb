class PricingService

  attr_reader :pricing_data, :error, :success

  def initialize(trip:)
    @trip = trip

    @pricing_data = {}
    @price_method = nil
    @success      = false
    @error        = nil
  end

  def run
    check_necessary_data
    get_pricing_data
    calculate_price
    calculate_intervals

    @success = true
  rescue => e
    # TODO: notificar a un admin para que lance el proceso de nuevo.
    pricing_logger.info "*************************************************"
    pricing_logger.info "Error running pricing service for trip #{@trip.id}: #{e.message}"
    pricing_logger.debug e.backtrace
    pricing_logger.info "*************************************************"
    @error = e.message
  end

  def calculate_negative_price(time_elapsed)
    time = time_elapsed || 15
    distance_km = @trip.gmaps_info.dig(:init_to_origin_info, :distance, :value) / 1000.0

    price = case @trip.price_info[:price_method]
            when "ida_espera"
              distance_km * @trip.price_info[:km_price]&.to_f + time * @trip.price_info[:time_price]&.to_f
            when "km_recorrido"
              distance_km * @trip.price_info[:km_price]&.to_f
            else
              distance_km * @trip.price_info[:km_price]&.to_f + time * @trip.price_info[:time_price]&.to_f
            end

    # selección del correcto
    price = [price, @trip.price_info[:min_price]&.to_f].max
    price = [price, @trip.price_info[:max_negative_price]&.to_f].min

    price
  end

  def check_necessary_data
    raise "El traslado no esta correctamente geolocalizado" unless @trip.gmaps_info.any?
    raise "El traslado no esta correctamente identificado" unless @trip.priced_by.present?
  end

  def get_pricing_data
    @price_method = @trip.customer.price_type || "ida_espera"

    @pricing_data = MainConfig.current.pricing_data
    @pricing_data.merge!(@trip.state_origin.pricing_data.compact)
    @pricing_data.merge!(@trip.city_origin.pricing_data.compact)
    @pricing_data.merge!(@trip.priced_by.pricing_data.compact) if @trip.priced_by_type == "Zone"

    use_long_data = @trip.long_trip? || @trip.mode == 'mce'

    @pricing_data[:price_method]  = @price_method
    @pricing_data[:distance_base] = use_long_data ? @pricing_data[:long_km_base] : @pricing_data[:km_base]
    @pricing_data[:min_salary]    = use_long_data ? @pricing_data[:min_salary_long_trip] : @pricing_data[:min_salary]

    @pricing_data[:min_salary]           ||= 49000.0
    @pricing_data[:minimal_wait]         ||= 0
    @pricing_data[:max_negative_price]   ||= 180.0
    @pricing_data[:min_waits]            ||= 0.0
    @pricing_data[:hour_start]           ||= "05:00"
    @pricing_data[:hour_end]             ||= "21:00"
    @pricing_data[:hour_band_coeficient] ||= 1.0
    @pricing_data[:minutes_per_day]      ||= 540
    @pricing_data[:variation_periods]    ||= 0

    @pricing_data[:km_price]   = @pricing_data[:distance_base] * @pricing_data[:kms_to_base_ratio]
    @pricing_data[:time_price] = @pricing_data[:min_base] * @pricing_data[:waits_to_base_ratio]

    @pricing_data[:max_customer_price] = get_max_price_from_customer

    @pricing_data
  end

  def calculate_price
    distance = @trip.distance
    time = @trip.best_guess_duration * time_coeficient + @pricing_data[:buffer_time]

    trip_price = case @price_method
    when "ida_espera"
      distance * @pricing_data[:km_price] + time * @pricing_data[:time_price]
    when "km_recorrido"
      distance * @pricing_data[:km_price]
    else
      distance * @pricing_data[:km_price] + time * @pricing_data[:time_price]
    end

    trip_price += (@pricing_data[:minimal_wait] * @pricing_data[:min_waits]) if @trip.mode == 'mce' && @price_method == "ida_espera"

    trip_price = [trip_price, (@pricing_data[:min_price] || 0)].max.round

    @pricing_data[:calculated_price] = trip_price
    @pricing_data[:extrapolated_salary] = if @trip.mode == 'mce'
      (22 * trip_price * (@pricing_data[:minutes_per_day]/(time * 2 + @pricing_data[:minimal_wait]))).round
    else
      (22 * trip_price * (@pricing_data[:minutes_per_day]/time)).round
    end

    @pricing_data[:coeficient] = (@pricing_data[:min_salary] / @pricing_data[:extrapolated_salary]).round(2)

    min_price, max_price = get_min_and_max_price

    @pricing_data[:variation_periods] = 0 if min_price == max_price
    @pricing_data.merge!(
      initial_price: min_price,
      final_price: max_price,
      current_price: @pricing_data[:variation_periods] > 0 ? min_price : max_price,
      initial_time: Time.zone.now,
      intervals: []
    )
  end

  def calculate_intervals
    return unless @trip.is_future? && @trip.return_time.blank?
    return unless @pricing_data[:variation_periods] > 0

    total_duration = @trip.time_to_send_to_external_provider.to_time - @pricing_data[:initial_time].to_time
    interval_duration = total_duration/(@pricing_data[:variation_periods] + 1)
    variation = (@pricing_data[:final_price] - @pricing_data[:initial_price])/@pricing_data[:variation_periods]

    next_time = @pricing_data[:initial_time]
    next_price = @pricing_data[:initial_price]

    @pricing_data[:variation_periods].times do
      next_time  = next_time + interval_duration
      next_price = (next_price + variation).round

      @pricing_data[:intervals] << { time:  next_time, price: next_price }
    end
  end

  def time_coeficient
    calculation_hour = (@trip.gmaps_info["calculation_hour"]&.to_time || @trip.created_at).strftime('%H:%M')

    trip_hour_type = if @trip.trip_hour > @pricing_data[:hour_start] && @trip.trip_hour <= @pricing_data[:hour_end]
      "diurno"
    else
      "nocturno"
    end

    calculation_hour_type = if calculation_hour > @pricing_data[:hour_start] && calculation_hour <= @pricing_data[:hour_end]
      "diurno"
    else
      "nocturno"
    end

    if trip_hour_type == calculation_hour_type
      1.0
    elsif trip_hour_type == "diurno"
      @pricing_data[:hour_band_coeficient]
    else
      (1/@pricing_data[:hour_band_coeficient]).round(2)
    end
  end

  def get_min_and_max_price
    if @pricing_data[:coeficient] >= 1
      max_price = @pricing_data[:calculated_price] * @pricing_data[:coeficient]
      min_price = max_price
    elsif @pricing_data[:coeficient] < 1
      min_price = @pricing_data[:calculated_price] * @pricing_data[:coeficient]
      max_price = @pricing_data[:calculated_price]
    end 

    min_price = [min_price, (@pricing_data[:min_price] || 0)].max.round
    max_price = min_price if min_price > max_price

    [min_price, max_price]
  end

  def get_max_price_from_customer
    return default_price_data unless @trip.customer.max_price_limited_by_feleval?

    service = CustomerApiService.new(trip: @trip)
    price_data = service.find_current_sell_price

    trip_price = case @trip.mode
    when 't', 'mse'
      price_data['section2']
    when 'mce'
      price_data['by_module']
    end

    negative_price = price_data['negative']
    wait_price = if @trip.mode == 'mce' && @trip.wait_authorized
      price_data['waits']
    else
      0.0
    end

    {
      trip:     trip_price&.to_f.presence || 0.0,
      negative: negative_price&.to_f.presence || 0.0,
      waits:    wait_price&.to_f.presence || 0.0
    }
  end

  def pricing_logger
    @pricing_logger ||= Logger.new("#{Rails.root}/log/pricing_service.log")
  end

  def default_price_data
    { trip: 0.0, negative: 0.0, waits: 0.0 }
  end
end
