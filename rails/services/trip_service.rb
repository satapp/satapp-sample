module Trips
  class TripService

    attr_reader :trip, :profiles, :fcm_service

    def initialize(trip:, recalculation: false)
      @trip = trip
      @recalculation = recalculation
    end

    def meeting_point_confirmation_by(user)
      new_address = GmapsService.new.reverse_geocode_point(@trip.meeting_point_coords)
      @trip.update(meeting_point: new_address)

      if !user.admin?
        pricing_service = PricingService.new(trip: @trip)
        max_distance = pricing_service.get_pricing_data[:max_distance_to_meeting_point] # in KM
        distance = (@trip.coords_origin.distance(@trip.meeting_point_coords))/1000

        CustomerMailer.notify_meeting_point_too_far(@trip.id).deliver_later if distance > (max_distance || 0)
      end

      leg = @trip.legs.in_progress.take

      return true if leg.nil?

      if leg.informed?
        leg.resolve_meeting_point_inform(user: user)
      else
        svc = leg.notify_driver("confirm_meeting_point")
        svc.success
      end
    rescue => e
      trip_service_logger.fatal "Error en #meeting_point_confirmation de trip #{@trip.id}: #{e.message}"
      trip_service_logger.debug e.backtrace
      false
    end

    def self.trips_for_driver(user_id, driver_profile)
      legs  = Leg.none

      # legs que están en el area de una base
      driver_profile.basis_areas.each do |area|
        result = Leg.publicable.includes(:trip).references(:trip)
          .where("ST_Covers(:area, trips.coords_origin)", area: area)

        legs = legs.or(result)
      end

      # legs inmediatas que están en el area actual del driver
      current_area = driver_profile.current_area
      if current_area
        result = Leg.immediate.includes(:trip).references(:trip)
          .where("ST_Covers(:area, trips.coords_origin)", area: current_area)

        legs = legs.or(result)
      end

      legs.distinct.order("trips.immediate DESC, trips.trip_date ASC, trips.hour_in_origin ASC")
    end

    def self.control_assignment(trip_id:)
      trip = Trip.find trip_id
      leg  = trip.leg == 'vuelta' ? trip.return_leg : trip.first_leg

      unless trip.provider_id.present? || leg.assigned?
        AlertsMailer.notify_unassigned_trip(trip_id: trip.id).deliver_later
        return
      end

      gmaps_service = GmapsService.new(route_info: trip.route_info)
      duration = gmaps_service.get_total_traffic_time_from(leg.user.last_known_position)

      raise "Duration cannot be blank" if duration.empty?

      time_in_origin = trip.time_in_origin&.to_datetime || trip.appointment_datetime

      second_control_time = time_in_origin - duration[:value].seconds - 10.minutes
      IdleTripAlarmJob.set(wait_until: second_control_time).perform_later(trip_id: trip.id, should_unassign: false)
    rescue => e
      trip_service_logger.fatal "Error en #control_assignment de trip #{trip.id}: #{e.message}"
      trip_service_logger.debug e.backtrace
      false
    end

    private

    def trip_service_logger
      @trip_service_logger ||= ::Logger.new("#{Rails.root}/log/trip_service.log")
    end
  end
end