# SATAPP
Descripción de los archivos incluidos en el repositorio

### Gemfile
Listado de plugins utilizado por el framework Rails

### package.json
Listado de plugins utilizado por el frontend Vue.js

### rails/controllers/invitations_controller.rb
Controller que se encarga de procesar las invitaciones en el backend. Modelo de controller usado en toda la aplicación

### jobs/assignment_control_job.rb
Background job que se ocupa de controlar la asignación de un trip en un momento dado.

### models/leg.rb
Model que contiene los tramos de un trip.
Implementa una máquina de estados para manejar las etapas de un tramo

### models/zone.rb
Model para contener el polígono de una zona geográfica para determinación de configuración de los trips incluidos en esa zona.

### services/location_service.rb
Servicio para el registro de la última posición conocida de un conductor, y la búsqueda de conductores según su última ubicación, para la notificación de traslados.

### services/pricing_service.rb
Servicio para el cálculo del precio de un trip, con sus escalas.

### services/trip_service.rb
Servicio para agrupar algunos métodos que involucran a un trip.

- **meeting_point_confirmation_by**: método que resuelve la confirmación del punto de encuentro de un trip por un usuario
- **trips_for_driver**: método que provee la lista de trips disponibles para un conductor
- **control_assignment**: método que chequea que un trip esté asignado a un conductor en un momento dado, genera alertas, y encola jobs para otros chequeos

### vue/*
Vistas construías utilizando Vue.js y el plugin Element.io
Se utiliza ese framework en aquellas pantallas donde la complejidad es mediana o alta.
